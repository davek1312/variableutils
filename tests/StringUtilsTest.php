<?php

namespace Davek1312\VariableUtils\Tests;

use Davek1312\VariableUtils\StringUtils;

class StringUtilsTest extends \PHPUnit_Framework_TestCase {

    public function testIsEmptyOrNull() {
        $this->assertTrue(StringUtils::isEmptyOrNull(''));
        $this->assertTrue(StringUtils::isEmptyOrNull(null));
        $this->assertFalse(StringUtils::isEmptyOrNull(0));
        $this->assertFalse(StringUtils::isEmptyOrNull(false));
        $this->assertFalse(StringUtils::isEmptyOrNull([]));
        $this->assertFalse(StringUtils::isEmptyOrNull('validstring'));
    }

    public function testStartsWith() {
        $string = 'abc';
        $startsWith = 'a';

        $this->assertFalse(StringUtils::startsWith(null, $startsWith));
        $this->assertFalse(StringUtils::startsWith('', $startsWith));
        $this->assertFalse(StringUtils::startsWith($string, ''));
        $this->assertFalse(StringUtils::startsWith($string, null));
        $this->assertFalse(StringUtils::startsWith($string, 'b'));


        $this->assertTrue(StringUtils::startsWith($string, $startsWith));
    }

    public function testEndsWith() {
        $string = 'abc';
        $endsWith = 'c';

        $this->assertFalse(StringUtils::endsWith(null, $endsWith));
        $this->assertFalse(StringUtils::endsWith('', $endsWith));
        $this->assertFalse(StringUtils::endsWith($string, ''));
        $this->assertFalse(StringUtils::endsWith($string, null));
        $this->assertFalse(StringUtils::endsWith($string, 'b'));

        $this->assertTrue(StringUtils::endsWith($string, $endsWith));
    }

    public function testStartsWithAndEndsWith() {
        $string = 'abc';
        $startsWith = 'a';
        $endsWith = 'c';

        $this->assertFalse(StringUtils::startsWithAndEndsWith(null, $startsWith, $endsWith));
        $this->assertFalse(StringUtils::startsWithAndEndsWith($string, 'b', $endsWith));

        $this->assertTrue(StringUtils::startsWithAndEndsWith($string, $startsWith, $endsWith));
    }

    public function testStartsWithAndEndsWithSame() {
        $string = 'aba';
        $startsAndEndsWith = 'a';

        $this->assertFalse(StringUtils::startsWithAndEndsWithSame(null, $startsAndEndsWith));
        $this->assertFalse(StringUtils::startsWithAndEndsWithSame($string, 'b'));

        $this->assertTrue(StringUtils::startsWithAndEndsWithSame($string, $startsAndEndsWith));
    }

    public function testContains() {
        $string = 'abc';

        $this->assertFalse(StringUtils::contains($string, null));
        $this->assertFalse(StringUtils::contains($string, ''));
        $this->assertFalse(StringUtils::contains($string, 'd'));

        $this->assertTrue(StringUtils::contains($string, 'a'));
        $this->assertTrue(StringUtils::contains($string, $string));
    }
}