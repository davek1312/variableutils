# davek1312/variableutils

Handy utility methods for PHP variables.

# Installation
`composer require davek1312/variableutils`

# Usage

## Strings
```php
<?php
use Davek1312\VariableUtils\StringUtils;

// Checks if $string is null or ''
StringUtils::isEmptyOrNull($string);

// Checks if $string starts with $startsWith
StringUtils::startsWith($string, $startsWith);

// Checks if $string ends with $endsWith
StringUtils::endsWith($string, $endsWith);

// Checks if $string starts with $startsWith and $string ends with $endsWith
StringUtils::startsWithAndEndsWith($string, $startsWith, $endsWith);

// Checks if $string starts with $startsAndEndsWith and $string ends with $startsAndEndsWith
StringUtils::startsWithAndEndsWithSame($string, $startsAndEndsWith);

// Checks if $string contains $contains
StringUtils::contains($string, $contains);
```