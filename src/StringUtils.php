<?php

namespace Davek1312\VariableUtils;

class StringUtils {

    /**
     * @param string $string
     *
     * @return boolean
     */
    public static function isEmptyOrNull($string) {
        return $string === null || $string === '';
    }

    /**
     * @param string $string
     * @param string $startsWith
     *
     * @return boolean
     */
    public static function startsWith($string, $startsWith) {
        $stringLength = strlen($string);
        $startsWithLength = strlen($startsWith);

        if($stringLength == 0 && $startsWithLength == 0) {
            return true;
        }
        if($stringLength > 0 && $startsWithLength == 0) {
            return false;
        }

        return (substr($string, 0, $startsWithLength) === $startsWith);
    }

    /**
     * @param string $string
     * @param string $endsWith
     *
     * @return boolean
     */
    public static function endsWith($string, $endsWith) {
        $stringLength = strlen($string);
        $endsWithLength = strlen($endsWith);

        if($stringLength == 0 && $endsWithLength == 0) {
            return true;
        }
        if($stringLength > 0 && $endsWithLength == 0) {
            return false;
        }
        return (substr($string, -$endsWithLength) === $endsWith);
    }

    /**
     * @param string $string
     * @param string $startsWith
     * @param string $endsWith
     *
     * @return boolean
     */
    public static function startsWithAndEndsWith($string, $startsWith, $endsWith) {
        return static::startsWith($string, $startsWith) && static::endsWith($string, $endsWith);
    }

    /**
     * @param string $string
     * @param string $startsAndEndsWith
     *
     * @return boolean
     */
    public static function startsWithAndEndsWithSame($string, $startsAndEndsWith) {
        return static::startsWithAndEndsWith($string, $startsAndEndsWith, $startsAndEndsWith);
    }

    /**
     * @param string $string
     * @param string $contains
     *
     * @return boolean
     */
    public static function contains($string, $contains) {
        if(strlen($contains) == 0) {
            return false;
        }

        return strpos($string, $contains) !== false;
    }
}